const { execSync } = require('child_process');
const { readJsonSync } = require('fs-extra');
const { join } = require('path');
const fs = require('fs');

console.log('\x1b[0K\x1b[35;1m***** Getting affected projects ... *****\x1b[0m');
const baseCommit = getBaseCommit();

const affectedProjects = getAffectedProjects(baseCommit, process.env.CI_COMMIT_SHA);

console.log('Found affected projects:');
console.log(affectedProjects.join('\n'));

console.log('\x1b[0K\x1b[35;1m***** Generating child pipelines ... *****\x1b[0m');

writeChildPipelineYaml(affectedProjects);

// ==================================================================================================
//  Helper functions
// ==================================================================================================

function getBaseCommit() {
  execSync('git fetch --tags');
  const baseTag = execSync('git describe --tags --abbrev=0').toString().trim();
  return execSync(`git rev-list -n 1 ${baseTag}`).toString().trim();
}

function getAffectedProjects(baseCommitSha, headCommitSha) {
  const printAffectedCmd = `npx nx print-affected --base=${baseCommitSha} --head=${headCommitSha}`;
  const affectedProjectsString = execSync(printAffectedCmd).toString();
  return JSON.parse(affectedProjectsString).projects;
}

function getProjectPath(project) {
  const packages = fs.readdirSync('packages');
  for (const pkg of packages) {
    const pkgJson = readJsonSync(join('packages', pkg, 'project.json'));
    if (pkgJson.name === project) {
      return `packages/${pkg}`;
    }
  }
}

function writeChildPipelineYaml(affectedProjectNames) {
  const childPipelineYml = 'child-pipeline-gitlab-ci.yml';

  // Add the parent pipeline template
  fs.appendFileSync(childPipelineYml, fs.readFileSync('tools/scripts/templates/project-pipeline.tpl.yml'));

  // Add the "no child pipelines" template if there are no affected projects
  if (affectedProjectNames.length === 0) {
    fs.appendFileSync(childPipelineYml, fs.readFileSync('tools/scripts/templates/no-child-pipelines.tpl.yml'));
  }

  // Add child pipelines for each affected project
  for (const affectedProject of affectedProjectNames) {
    const projectName = affectedProject;
    const projectPath = getProjectPath(affectedProject);
    const childPipelineTpl = fs.readFileSync('tools/scripts/templates/child-pipeline.tpl.yml', { encoding: 'utf-8' });
    const childPipeline = childPipelineTpl.replace(/__PATH__/g, projectPath).replace(/__PROJECT_NAME__/g, projectName);
    fs.appendFileSync(childPipelineYml, childPipeline);
  }
}
