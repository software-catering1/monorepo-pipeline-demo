import { isOdd } from '@softwarecatering/is-odd';

export function isEven(): boolean {
  // some change
  return !isOdd();
}
